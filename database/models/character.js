'use strict';
module.exports = (sequelize, DataTypes) => {

  const Character = sequelize.define('Character', {
    CharacterId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    Name: DataTypes.STRING,
    Xp: DataTypes.INTEGER
  }, {
    timestamps: false,
  });

  Character.associate = function(models) {
    Character.belongsTo(models.Account, {
      foreignKey: 'AccountId',
      allowNull: false,
    });
  };

  return Character;

};