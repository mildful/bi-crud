'use strict';

module.exports = (sequelize, DataTypes) => {

  const Account = sequelize.define('Account', {
    AccountId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: false,
  });

  Account.associate = function(models) {
    // associations can be defined here
  };

  return Account;

};