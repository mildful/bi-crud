import * as Handlers from './handlers';

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const handle = handler =>
  (req, res, next) => {
    Promise.resolve(handler(req, res, next)).catch(next);
  }

export function registerResource(router, name) {
    name = name.toLowerCase();
    const handlerName = capitalizeFirstLetter(name) + 'Handler';
    const handler = Handlers[handlerName];

    if (!handler) {
        throw new Error(`Handler ${handlerName} was not found.`);
    }

    router.route(`/${name}`)
        .get(handle(handler.getAll))
        .post(handle(handler.create));

    router.route(`/${name}/:id`)
        .get(handle(handler.get))
        .put(handle(handler.update))
        .delete(handle(handler.delete));
}
