import { Router } from 'express';
import { registerResource } from './resource';

const router = Router();

// If we want a scalable solution, we might go with something like this.
// But the real solution is to use existing libraries or frameworks.
// This approach is definitly convention over configuration.
// If we really want  our own solution, it may be a good idea to provide a CLI to generate handlers.
registerResource(router, 'account');
registerResource(router, 'character');

// OR uncomment code below

/*
const handle = handler =>
  (req, res, next) => {
    Promise.resolve(handler(req, res, next)).catch(next);
  }

router.route('/account')
  .get(handle(AccountHandler.getAll))
  .post(handle(AccountHandler.create));

router.route('/account/:accountId')
  .get(handle(AccountHandler.get))
  .put(handle(AccountHandler.update))
  .delete(handle(AccountHandler.delete));
*/

export default router;
