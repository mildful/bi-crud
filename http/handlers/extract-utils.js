import { HttpError } from "../errors";

export default (from, ...toExtract) => {
    let res = {};
    toExtract.forEach(key => {
        const value = from[key];
        console.log(key, value)
        if (!value) {
            throw new HttpError(`Argument ${key} not found.`, 400);
        }
        res[key] = value;
    });
    return res;
}
