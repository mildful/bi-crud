import { HttpError } from '../errors';
import { Character, Account } from '../../database/models';
import extract from './extract-utils';

export default {
    
    async getAll(req, res, next) {
        try {
            res.json(await Character.all());
        } catch(err) {
            throw new HttpError('Error during querying.', 500);
        }
    },

    async create(req, res, next) {
        const { name, xp, accountId } = extract(req.body, 'name', 'xp', 'accountId');
        let account = null;

        try {
            account = await Account.findById(accountId);
        } catch(err) {
            throw new HttpError('Error during querying.', 500);
        }

        if (account === null) {
            throw new HttpError(`Account with id ${accountId} does not exists.`, 404);
        }
        
        try {
            res.json(await Character.create({
                Name: name,
                Xp: xp,
                AccountId: accountId,
            }));
        } catch(err) {
            throw new HttpError('Error during querying.', 500);
        }
    },

    async get(req, res, next) {
        const { id } = req.params;
        let character = null;

        try {
            character = await Character.findById(id);
        } catch(err) {
            throw new HttpError('Error during querying.', 500);
        }

        if (character === null) {
            throw new HttpError('Account not found.', 404);
        }

        res.json(character);
    },

    async update(req, res, next) {
        const { id } = req.params;
        const { name, xp, accountId } = extract(req.body, 'name', 'xp', 'accountId');

        try {
            await Character.update({
                Name: name,
                Xp: xp,
                AccountId: accountId,
            }, {
                where: { CharacterId: id }
            })
            // .update() only returns the number of affected rows.
            // it can returns the affected rows only with a postgre db.
            // so here we need to query it manually
            const newCharacter = await Character.findById(id);
            res.json(newCharacter);
        } catch (err) {
            throw new HttpError('Error during querying.', 500);
        }
    },

    async delete(req, res, next) {
      const { id } = req.params;

      try {
        await Character.destroy({ where: { CharacterId: id } });
      } catch(err) {
        throw new HttpError('Error during querying.', 500);
      }

      res.json({ success: true });
    },

}
