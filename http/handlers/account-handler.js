import { Account } from '../../database/models';
import { HttpError } from '../errors';

export default {
    
    async getAll(req, res, next) {
      try {
        res.json(await Account.all());
      } catch(err) {
        throw new HttpError('Error during querying.', 500);
      }
    },

    async create(req, res, next) {
      try {
        res.json(await Account.create());
      } catch(err) {
        throw new HttpError('Error during querying.', 500);
      }
    },

    async get(req, res, next) {
      const { id } = req.params;

      let account = null;

      try {
        account = await Account.findById(id);
      } catch(err) {
        throw new HttpError('Error during querying.', 500);
      }

      if (account === null) {
        throw new HttpError('Account not found.', 404);
      }

      res.json(account);
    },

    async update(req, res, next) {
      throw new HttpError('You cannot update an account.', 403);
    },

    async delete(req, res, next) {
      const { id } = req.params;

      try {
        await Account.destroy({ where: { AccountId: id } });
      } catch(err) {
        throw new HttpError('Error during querying.', 500);
      }

      res.json({ success: true });
    },

}
