export class HttpError extends Error {

  constructor(msg, code = 500) {
    super(msg);
    this.code = code;
  }

}

export function logError(err, req, res, next) {
  console.error(err.stack);
  next(err);
}

export function clientErrorHandler(err, req, res, next) {
  if (req.headers.accept.match(/(json)|(\*\/\*)/)) {
    const code = err.code || 500;
    res.status(code).send({ error: err.message });
  } else {
    next(err);
  }
}

export function errorHandler(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  res.status(500);
  res.render('error', { error: err });
}
