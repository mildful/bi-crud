# Setup

- installer les packages via `yarn install`
- créer la db locale via `yarn db:init`
- lancer le serveur avec `yarn start`

Enjoy.

# Notes

La solution doit être scalable : j'ai donc décidé d'utiliser un ORM nommé sequelize.

Il offre tout ce qu'on un ORM de base : un fichier de config, des migrations pour facilement faire évoluer le modèle de donnée, des models pour générer de requêtes via une interface objet et des seeders (que je n'ai pas utilisé dans ce projet) pour rapidement peupler une base de test.

La base utilisée est celle de développement.

Les logs sont activés, permettant ainsi de voir les requêtes jouées dans la console.

Projet testé avec node 10.7.x et la dernière version de MariaDB (windows 10 64bits).
