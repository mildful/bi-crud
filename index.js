import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routerApi from './http/router-api';
import { logError, clientErrorHandler, errorHandler } from './http/errors';

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 1337;

app.use('/api', routerApi);

app.use(logError);
app.use(clientErrorHandler);
app.use(errorHandler);

app.listen(port);
console.log('Server started on ' + port + '.');
